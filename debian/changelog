cowpoke (0.4) unstable; urgency=low

  * Simplify things to start with reduced privileges and only escalate them
    to run cowbuilder, rather than starting as root and dropping them for a
    few operations.  This means people can configure sudo to only permit the
    user(s) to invoke cowbuilder with it.
  * Don't explicitly pass a buildd username unless it is set, this allows
    people to configure the remote username in their ssh config or similar.
    Closes: #538191
  * Supply pbuilder with the --buildplace and --aptcache options to better
    enable use with just these reduced privileges.
  * Handle relative paths a bit more sensibly, they are all relative to the
    the BUILDD_USER's home directory now, not the incoming dir.
  * Add --dpkg-opts support for passing options on to dpkg.
  * Autoclean the apt-cache so it doesn't grow unbounded.

 -- Ron Lee <ron@debian.org>  Fri, 31 Jul 2009 17:01:01 +0930

cowpoke (0.3) unstable; urgency=low

  * Add multiple arch/dist support.

 -- Ron Lee <ron@debian.org>  Sat, 01 Nov 2008 02:49:24 +1030

cowpoke (0.2) unstable; urgency=low

  * Permit the use of an explicit path in the .dsc name passed.
  * Add support for BUILDD_ROOTCMD, for people who prefer sudo to secure keys
    and a direct root log in.

    Many thanks to Cyril Brulebois for the feedback and patches for this.

 -- Ron Lee <ron@debian.org>  Sat, 03 May 2008 01:52:59 +0930

cowpoke (0.1) unstable; urgency=low

  * Initial Release.  Closes: #478167

 -- Ron Lee <ron@debian.org>  Mon, 28 Apr 2008 02:05:53 +0930
